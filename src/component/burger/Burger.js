import React from 'react'

import Classes from '../burger/Burger.module.css'
import BurgerIngredient from './BurgerIngredient/Burgeringredient'

const Burger = (props) => {
    let transformedIngredients = Object.keys(props.Ingredients).map(igKey => {
        return[...Array(props.Ingredients[igKey])].map((_, i) => {
            return <BurgerIngredient key={igKey + i} type={igKey} />
        })
    })
    .reduce((arr, el) => {
        return arr.concat(el)
    }, []); 
    if(transformedIngredients.length === 0){
        transformedIngredients = <p>Please starts adding ingredient</p>  
    }
    console.log(transformedIngredients);
    // console.log(Object.keys(props.Ingredients));
    return(
        <div className={Classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {transformedIngredients}
            <BurgerIngredient type="bread-bottom" />
        </div>
    )
}

export default Burger;