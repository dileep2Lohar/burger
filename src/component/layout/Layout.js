import React from 'react'
import Auxx from "../../hoc/auxx";
import Classes from '../layout/Layout.module.css'


const Layout = (props) => {
    return(
        <Auxx>
            <div>toolbar, sidebar, backdrop</div>
            <main className={Classes.content}>{props.children}</main>
        </Auxx>
    )
}
export default Layout;