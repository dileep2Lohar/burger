
import React, { Component } from 'react'
import Auxx from "../../hoc/auxx";
import Burger from "../../component/burger/Burger";


class BurgerBuilder extends Component{
    // constructor(props){
    //     super(props)
    //     this.state = {...}
    // }
    state = {
        Ingredients: {
            salad :0,
            bacon: 0,
            cheese: 0,
            meat: 0
        }
    }
    render(){
        console.log(this.state.Ingredients);
        return(
            <Auxx>
                <Burger Ingredients = {this.state.Ingredients} />
                <div>Burger Controls</div>
            </Auxx>
        )
    }
}
export default BurgerBuilder;